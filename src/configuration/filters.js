import { refactorNumber } from '@/utils/pureFunctions'
// import {moneyToString} from 'short-money'

export default {
  parseDate(val) {
    const date = new Date(val)
    return date.toLocaleDateString('RU-ru')
  },
  money(val) {
    const num = val ? Math.round(val * 100) / 100 : null
    return num ? num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ') : 0
  },
  hidePartPhone(val) {
    if (!val) return ''
    const phoneNumber = refactorNumber({ prefix: '+998 ', removeSymbol: '-', number: val }).split('')
    phoneNumber.splice(7, 3, ' * * * ')
    return phoneNumber.join('')
  }

  // numberToString(value) {
  //   return moneyToString(value)
  // }

}
