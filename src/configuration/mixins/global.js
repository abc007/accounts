const errorsFromServer = {
  'errors.un_authorized': 'Неправильный пароль и логин',
  'errors.field_value_unique': 'Данные должны быть уникальными'
}

export default {
  methods: {
    /**
     *
     * @param permission {string}
     * @return {boolean}
     */
    $can(permission) {
      return this.$store.getters.permissions.includes(permission)
    },
    $showLoading() {
      this.$store.commit('setLoading', true)
    },
    $clearLoading() {
      this.$store.commit('setLoading', false)
    },
    $setErrors({ title = 'Ошибка', text = 'Произошла ошибка' }) {
      this.$notify({
        title,
        text,
        type: 'error'
      })
    },
    $setResponseError({ title = 'Ошибка', error }) {
      const text = error.response &&
      error.response.data &&
      error.response.data.code &&
      errorsFromServer[error.response.data.code]
        ? errorsFromServer[error.response.data.code]
        : error.message
      this.$notify({
        title,
        text,
        type: 'error'
      })
    },
    $setSuccess({ title, text, type }) {
      this.$notify({
        title: title,
        text: text,
        type
      })
    },
    $setFormError({ error, obs }) {
      if (obs) {
        const errors = {}
        if (error.response && error.response.data && error.response.data.params) {
          let message = error.message
          if (errorsFromServer[error.response.data.code]) message = errorsFromServer[error.response.data.code]
          error.response.data.params.forEach(item => {
            errors[item] = message
          })
        }
        obs.setErrors(errors)
      } else {
        console.warn(`Observer not found`)
      }
    }
  },
  computed: {
    console: () => console,
    errorsFromServer: () => errorsFromServer
  }
}
