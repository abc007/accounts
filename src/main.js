import Vue from 'vue'
import Cookies from 'js-cookie'
import 'normalize.css/normalize.css'

import Element from 'element-ui'
import './styles/element-variables.scss'
import enLang from 'element-ui/lib/locale/lang/en'
import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'
import VeeValidate from './plugins/vee-validate/vee-validate'
import http from '@/plugins/axios'
import Configuration from './configuration/index'
import TokenService from '@/utils/TokenService'

import './icons' // icon
import './permission' // permission control
import './utils/error-log' // error log

import * as filters from './filters' // global filters

if (process.env.NODE_ENV === 'production') {
  const { mockXHR } = require('../mock')
  mockXHR()
}

Vue.use(Element, {
  size: Cookies.get('size') || 'medium', // set element-ui default size
  locale: enLang
})
Vue.prototype.$http = http
Vue.use(Configuration)
Vue.use(VeeValidate)

Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.config.productionTip = false;

(async function() {
  const token = TokenService.getToken()
  if (token) {
    try {
      await store.dispatch('getUser')
      // await store.dispatch('setTimeIntervalRefreshToken')
    } catch (e) {
      console.error('Failed to get user data')
    }
  }
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
})()
