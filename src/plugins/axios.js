import Axios from 'axios'
import TokenService from '../utils/TokenService'
import router from '../router'
import store from '../store'

const baseURL = process.env.VUE_APP_API_URL
export const http = Axios.create({
  baseURL
})

function setConfiguration(provider) {
  provider.interceptors.request.use(config => {
    const token = TokenService.getToken()

    if (token) {
      config.headers['Authorization'] = `Bearer ${token}`
    }
    config.headers['Accept'] = 'application/json'
    config.headers['Content-Type'] = 'application/json'
    return config
  },
  error => Promise.reject(error)
  )
  provider.interceptors.response.use(res => res,
    error => {
      if (error.response && (error.response.status === 401 || error.response.status === 403)) {
        TokenService.removeToken()
        TokenService.removeRefreshToken()
        store.commit('clearUser')
        if (router.history.current.name !== 'login') {
          router.push({ name: 'login' })
        }
      }
      return Promise.reject(error)
    })
}

setConfiguration(http)

export default http
