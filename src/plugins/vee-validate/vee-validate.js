import { ValidationObserver, ValidationProvider, extend, setInteractionMode } from 'vee-validate'
import { email, required, min } from 'vee-validate/dist/rules'
import { messages } from './message.json'
// rules
extend('required', {
  ...required,
  message: messages.required
})

extend('card_number', {
  validate(value) {
    return value.replace(/-/g, '').length === 16
  },
  message: 'Номер карты недействительный'
})

extend('phone', {
  validate(value) {
    const pattern = /\d{2}-?\d{3}-?\d{2}-?\d{2}/
    return pattern.test(value)
  },
  message: '{_field_} введено неправильно'
})

extend('inn', {
  validate(value) {
    if (!value) return false
    return value.length === 9
  },
  message: '{_field_} не действительный'
})
extend('pnfl', {
  validate(value) {
    if (!value) return false
    return value.length === 14
  },
  message: '{_field_} не действительный'
})

extend('email', {
  ...email,
  message: messages.email
})

extend('min_value', {
  params: ['min'],
  validate(value, { min }) {
    if (!value) return true
    return parseInt(value.replace(/\s/g, '')) >= min
  },
  message: messages.min_value
})
extend('min', {
  ...min,
  message: messages.min
})
extend('max_value', {
  params: ['max'],
  validate(value, { max }) {
    if (!value) return true
    return parseInt(value.replace(/\s/g, '')) <= max
  },
  message: messages.max_value
})

extend('confirmed', {
  params: ['value', 'fieldName'],
  validate(value, { value: fieldValue }) {
    return value === fieldValue
  },
  message: 'Пароли не совпадают'
})

// rules

setInteractionMode('lazy')
export default {
  install(Vue) {
    Vue.component('ValidationProvider', ValidationProvider)
    Vue.component('ValidationObserver', ValidationObserver)
  }
}
