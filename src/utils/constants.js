export const messages = {
  onDelete: 'Успешно удалено!',
  onSave: 'Успешно сохранено!',
  selectMkoBeforeCreateCard: 'Чтобы добавить карту пожалуйста веберите МКО!',
  onAddedCard: 'Карта успешно добавлено!',
  onRejectedCredit: 'Кредит успешно отменено!',
  onConfirmedCredit: 'Кредит успешно активировано!',
  sendNotification: 'Сообщение отправлено!',
  onCloseCredit: 'Кредит успешно закрылся!'
}

export const CURRENCY_TYPE = {
  860: 'Сум',
  840: 'Доллар',
  978: 'Евро'
}
export const CURRENCIES = {
  860: 860, // som
  840: 840, // dollar
  978: 978
}

export const ROLES = {
  CM_ADMIN: 'SYS_ADMIN',
  CM_ADMINS: 'CM_ADMIN'
}

export const ORDER_GROUP_STATUSES = new Proxy(
  {
    NEW: {
      text: 'новый',
      color: '#48BB78',
      key: 'NEW'
    },
    IN_PROGRESS_BY_OPERATOR: { text: 'Ожидание', color: '#F2994A', key: 'IN_PROGRESS_BY_OPERATOR' },
    COMPLETED_BY_COLLECTOR: { text: 'Завершено', color: '#74797D', key: 'COMPLETED_BY_COLLECTOR' },
    COMPLETED_IN_ADVANCE: { text: 'Завершено Заранее', color: '#F23D5B', key: 'COMPLETED_IN_ADVANCE' },
    LACK_OF_MONEY: { text: 'Завершено с нехваткой денег', color: '#F23D5B', key: 'LACK_OF_MONEY' },
    IN_PROGRESS_BY_COLLECTOR: { text: 'В процессе', color: '#F23D5B', key: 'IN_PROGRESS_BY_COLLECTOR' },
    PRE_COMPLETED: { text: 'Завершено Заранее', color: '#F23D5B', key: 'PRE_COMPLETED' }
  },
  {
    get(target, name) {
      return name in target ? target[name] : { text: '', color: '', key: '' }
    }
  }
)

export const ORDER_STATUSES = new Proxy(
  {
    NEW: {
      text: 'новый',
      color: '#48BB78',
      key: 'NEW'
    },
    WAITING: { text: 'Ожидание', color: '#F2994A', key: 'WAITING' },
    COMPLETED_BY_COLLECTOR: { text: 'Завершено c инкассатаром', color: '#74797D', key: 'COMPLETED_BY_COLLECTOR' },
    COMPLETED_BY_OPERATOR: { text: 'Завершено c оператором', color: '#74797D', key: 'COMPLETED_BY_OPERATOR' },
    IN_PROGRESS: { text: 'В процессе', color: '#2684FF', key: 'IN_PROGRESS' },
    LACK_OF_MONEY: { text: 'Завершено с нехваткой денег', color: '#F23D5B', key: 'LACK_OF_MONEY' },
    COMPLETED: { text: 'Завершено', color: '#F23D5B', key: 'COMPLETED' }
  },
  {
    get(target, name) {
      return name in target ? target[name] : { text: '', color: '', key: '' }
    }
  }
)
export const ORDER_GROUP_STATUS_KEYS_WITHOUT_NEW = Object.keys(ORDER_GROUP_STATUSES).filter(item => item !== ORDER_GROUP_STATUSES.NEW.key)

export const PERMISSIONS = {
  'IS_SYS_ADMIN': 'IS_SYS_ADMIN',
  'USER': 'USER',

  'AGENT': 'AGENT',
  'MACHINE': 'MACHINE',
  'ROLE': 'ROLE',

  'PERMISSION': 'PERMISSION',

  'CAR': 'CAR',
  'CAR_MODIFY': 'CAR_MODIFY',
  'CAR_DELETE': 'CAR_DELETE',

  'CASSETTE': 'CASSETTE',

  'ORDER': 'ORDER',
  'ORDER_MODIFY': 'ORDER_MODIFY',
  'ORDER_CHANGE_NUMBER': 'ORDER_CHANGE_NUMBER',

  'OFFER': 'OFFER',

  'ORDER_GROUP': 'ORDER_GROUP',
  'ORDER_GROUP_FOR_OPERATOR': 'ORDER_GROUP_FOR_OPERATOR',
  'ORDER_GROUP_FOR_COLLECTOR': 'ORDER_GROUP_FOR_COLLECTOR',
  'ORDER_GROUP_START_OPERATOR': 'ORDER_GROUP_START_OPERATOR',
  'ORDER_GROUP_START_COLLECTOR': 'ORDER_GROUP_START_COLLECTOR',
  'ORDER_GROUP_MUST_END_DEPOSIT': 'ORDER_GROUP_MUST_END_DEPOSIT',
  'ORDER_GROUP_END_DEPOSIT': 'ORDER_GROUP_END_DEPOSIT',

  'OWN_CASSETTE_MODIFY': 'OWN_CASSETTE_MODIFY',
  'OWN_CASSETTE_DELETE': 'OWN_CASSETTE_DELETE'
}

export const CASSETTE_STATUS_TYPES = {
  FREE: 'Свободно',
  USED: 'Используется',
  BROKEN: 'Сломан'
}

export const FORECAST_TYPE = {
  IN_ONE_DAY: 'IN_ONE_DAY',
  IN_THREE_DAY: 'IN_THREE_DAY',
  IN_FIVE_DAY: 'IN_FIVE_DAY'
}
