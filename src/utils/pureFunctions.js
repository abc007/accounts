export function generateText(wordCount = 10) {
  let text = ''
  const alphabet = 'A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,r,s,t,u,v,w,x,y,z'.split(',')
  for (let i = 0; i < wordCount; i++) {
    const rand = Math.floor(Math.random() * alphabet.length)
    text += alphabet[rand]
  }
  return text
}

export function refactorNumber({ prefix, number, removeSymbol }) {
  if (removeSymbol) {
    number = number.replace(new RegExp(removeSymbol, 'ig'), '')
  }
  return prefix + number
}

export function debounce(fn, delay) {
  let timeout = null
  return function(...args) {
    clearTimeout(timeout)
    timeout = setTimeout(() => {
      fn.apply(this, args)
    }, delay)
  }
}

export const makerPagination = ({ limit, total }) => {
  return {
    limit,
    total,
    lastPage: Math.ceil(total / limit)
  }
}

export const isValidDate = date => {
  return !isNaN(new Date(date).getTime())
}

export const removerSpace = str => {
  if (!str) return ''
  if (typeof str === 'number') return str
  return str.replace(/\s/g, '')
}

export const alphaOnly = () => {
  const isAlphabet = /[а-яА-Яa-zA-z\s]/i.test(event.key)
  if (isAlphabet) return true
  else event.preventDefault()
}

export const numberOnly = () => {
  const isNumber = /[\d]/i.test(event.key)
  if (isNumber) return true
  else event.preventDefault()
}

export const getMonthByNumber = (number) => {
  const months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']
  return months[number - 1]
}

export const setThemeToLocalStorage = (theme, key = 'theme') => {
  localStorage.setItem(key, theme)
}

export const getThemeFromLocalStorage = (key = 'theme', defaultTheme = 'light') => {
  return localStorage.getItem(key) || defaultTheme
}

export const parseTextToHtml = text => {
  const pattern = /<body[^>]*>((.|[\n\r])*)<\/body>/im
  text = text.split(pattern)
  return text[1]
}

export const downloadTextFile = (filename, text) => {
  const element = document.createElement('a')
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' +
    encodeURIComponent(text))
  element.setAttribute('download', filename)
  element.style.display = 'none'
  document.body.appendChild(element)
  element.click()
  document.body.removeChild(element)
}
export const downloadBlobFile = (blob, fileName = 'file') => {
  const a = document.createElement('a')
  document.body.appendChild(a)
  a.style.display = 'none'
  const url = window.URL.createObjectURL(blob)
  a.href = url
  a.download = fileName
  a.click()
  window.URL.revokeObjectURL(url)
  a.remove()
}

export const wrapItem = (item, wrapper) => {
  wrapper = wrapper || document.createElement('div')
  item.parentNode.append(wrapper)
  wrapper.appendChild(item)
}

export const trueSetString = val => {
  return val ? '1' : '0'
}

export const getObjectValuesByKey = (obj1, obj2) => {
  return Object.entries(obj1).reduce((initial, [key, value]) => {
    if (key in obj2) {
      initial[key] = obj2[key]
    } else {
      initial[key] = value
    }
    return initial
  }, {})
}
export const shallowEqual = (object1, object2) => {
  const keys1 = Object.keys(object1)
  const keys2 = Object.keys(object2)
  if (keys1.length !== keys2.length) {
    return false
  }
  for (const key of keys1) {
    if (object1[key] !== object2[key]) {
      return false
    }
  }
  return true
}

export const clearSpace = text => {
  return text.replace(/\s/g, '')
}

export const getQueryPage = (page, defaultPage = 1) => {
  if (isNaN(page)) return defaultPage
  return +page
}
