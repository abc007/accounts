import axios from 'axios'

const TOKEN_KEY = 'Authorization'
const REFRESH_TOKEN_KEY = 'Refresh'
const EXPIRE = 'Expire'

const TokenService = {
  getToken() {
    return localStorage.getItem(TOKEN_KEY)
  },
  setToken(token, expire) {
    localStorage.setItem(TOKEN_KEY, token)
    if (expire) {
      expire = new Date().getTime() + expire * 1000
      localStorage.setItem(EXPIRE, expire)
    }
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
  },
  checkExpireIfOutdatedDelete() {
    const expire = localStorage.getItem(EXPIRE)
    if (new Date(parseInt(expire)).getTime() <= new Date().getTime()) {
      localStorage.removeItem(TOKEN_KEY)
    }
  },
  removeToken() {
    localStorage.removeItem(TOKEN_KEY)
    localStorage.clear()
  },
  removeRefreshToken() {
    localStorage.removeItem(REFRESH_TOKEN_KEY)
  }
}

export default TokenService
