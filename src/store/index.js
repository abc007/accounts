import Vue from 'vue'
import Vuex from 'vuex'
import { getField, updateField } from 'vuex-map-fields'
import http from '@/plugins/axios'
import TokenService from '@/utils/TokenService'
import { ROLES } from '@/utils/constants'

Vue.use(Vuex)

const modulesFiles = require.context('./modules', true, /\.js$/)
const modules = modulesFiles.keys().reduce((modules, modulePath) => {
  const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
  const value = modulesFiles(modulePath)
  modules[moduleName] = value.default
  return modules
}, {})

const store = new Vuex.Store({
  state: {
    me: null,
    controlValue: {
      showLayout: false,
      floatLayout: false,
      enableDownload: true,
      previewModal: false,
      filename: 'ofb',
      paginateElementsByHeight: 1100,
      pdfQuality: 2,
      pdfFormat: 'a4',
      pdfOrientation: 'portrait',
      pdfContentWidth: '845px',
      manualPagination: false
    }
  },
  getters: {
    permissions: state => {
      return state.me?.permissions || []
    },
    isUserFromMainBranch(state) {
      if (state.me) {
        return state.me.isMainBranch
      }
      return false
    },
    isAdmin: state => {
      if (state.me) {
        return state.me.roles.find(item => item === ROLES.CM_ADMINS)
      }
      return false
    },
    me: state => state.me,
    role: state => {
      if (state.me) {
        return state.me.roles[0]
      }
      return null
    },
    getField,
    sidebar: state => state.app.sidebar,
    size: state => state.app.size,
    device: state => state.app.device,
    visitedViews: state => state.tagsView.visitedViews,
    cachedViews: state => state.tagsView.cachedViews,
    token: state => state.user.token,
    avatar: state => state.user.avatar,
    name: state => state.user.name,
    introduction: state => state.user.introduction,
    roles: state => state.user.roles,
    permission_routes: state => state.permission.routes,
    errorLogs: state => state.errorLog.logs
  },
  mutations: {
    clearUser(state) {
      state.me = null
    },
    updateField
  },
  actions: {
    async login({ state, dispatch }, user) {
      try {
        const { data: { token }} = await http.post('login', {
          ...user
        })
        TokenService.setToken(token)
      } catch (e) {
        return Promise.reject(e)
      }
    },
    async getUser({ state }) {
      try {
        const { data } = await http.post('get-user')
        if (data) {
          localStorage.setItem('role', 'admin')
          state.me = data
          return Promise.resolve()
        }
      } catch (e) {
        return Promise.reject(e)
      }
    }
  },
  modules
})

export default store
